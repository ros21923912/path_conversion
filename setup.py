from setuptools import find_packages, setup
package_name = 'path_conversion'

from generate_parameter_library_py.setup_helper import generate_parameter_module

generate_parameter_module(
    "path_params",
    "config/config.yaml"
)

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Steven',
    maintainer_email='koroyeldiores@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'lane_converter = path_conversion.lane_converter:main'
        ],
    },
)
