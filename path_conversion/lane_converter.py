import rclpy
from rclpy.node import Node
from lanelet_msgs.msg import Lanelet
from geometry_msgs.msg import PoseStamped,Point,Quaternion
from nav_msgs.msg import Path
from path_conversion.path_params import conversion_params

import numpy as np
from numpy.polynomial.polynomial import Polynomial
from scipy.interpolate import splrep, BSpline


def create_point(x_axe:float,y_axe:float,z_axe:float) -> Point:
    point=Point()
    point.x=x_axe
    point.y=y_axe
    point.z=z_axe
    return point

def create_quaternions(real:float,imagin_x:float,imagin_y:float,imagin_z:float) -> Quaternion:
    quaternion=Quaternion()
    quaternion.w=real
    quaternion.x=imagin_x
    quaternion.y=imagin_y
    quaternion.z=imagin_z
    return quaternion

def create_poseStamped(current_point:Point,orientation:Quaternion) -> PoseStamped:
    poseStamped=PoseStamped()
    poseStamped.pose.position=current_point
    poseStamped.pose.orientation=orientation
    return poseStamped

def quaternionsFromPoints(point1:Point,point2:Point) -> Quaternion:
    point_A=np.array([point1.x,point1.y,point1.z])
    point_B=np.array([point2.x,point2.y,point2.z])
    vector_AB=point_A-point_B
    vector_AB_normalized = vector_AB / np.linalg.norm(vector_AB)
    theta = np.arccos(
        np.dot(point_A, vector_AB) / (np.linalg.norm(point_A) * np.linalg.norm(vector_AB))
        )
    sin_half_theta =np.sin(theta / 2)
    return create_quaternions(
        np.cos(theta / 2),
        sin_half_theta * vector_AB_normalized[0],
        sin_half_theta * vector_AB_normalized[1],
        sin_half_theta * vector_AB_normalized[2]
    )
def middlePoint(point1:Point,point2:Point) -> Point:
        return create_point(
            (point1.x+point2.x)/2,
            (point1.y+point2.y)/2,
            (point1.z+point2.z)/2)

def y_smoother(y_points:list) -> list:
    pass

class PathPlanning(Node):
    def __init__(self):
        super().__init__('lanelet_path_conversion')
        param_listener=conversion_params.ParamListener(self)
        params = param_listener.get_params()
        self.publisher=self.create_publisher(
            Path,
            params.topic_publisher_name,
            params.queue_size_publish
            )
        self.subscriber = self.create_subscription(
            Lanelet,
            params.topic_subscriber_name,
            self.generate_path, 
            params.queue_size_subscribe
            )
        self.curve_degree=params.curve_degree
    def generate_path(self,msg:Lanelet) -> None:
        path_points = Path()
        path_points.header.stamp = self.get_clock().now().to_msg()
        path_points.header.frame_id="My_id"
        array_length=len(msg.left_boundary.line_string)
        
        for iter in range(array_length):
            tempPoint=middlePoint(msg.left_boundary.line_string[iter],msg.right_boundary.line_string[iter])
            path_points.poses.append(
                create_poseStamped(
                    tempPoint,
                    quaternionsFromPoints(
                        middlePoint(
                            msg.left_boundary.line_string[iter+1],
                            msg.right_boundary.line_string[iter+1]
                            )if (iter+1 < array_length) else middlePoint(
                                create_point(
                                    (msg.left_boundary.line_string[iter].x)*2-msg.left_boundary.line_string[iter-1].x,
                                    (msg.left_boundary.line_string[iter].y)*2-msg.left_boundary.line_string[iter-1].y,
                                    (msg.left_boundary.line_string[iter].z)*2-msg.left_boundary.line_string[iter-1].z),
                                create_point(
                                    (msg.right_boundary.line_string[iter].x)*2-msg.right_boundary.line_string[iter-1].x,
                                    (msg.right_boundary.line_string[iter].y)*2-msg.right_boundary.line_string[iter-1].y,
                                    (msg.right_boundary.line_string[iter].z)*2-msg.right_boundary.line_string[iter-1].z),
                                    ),tempPoint)))
        self.publisher.publish(path_points)


def main(args=None) -> None:
    rclpy.init(args=args)       
    pathPlanner = PathPlanning()
    
    rclpy.spin(pathPlanner)
    pathPlanner.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
