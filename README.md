# Path Conversion
Here the algorithm is called which generates the path from two point lists, which consists of determined points with orientation in quaternions.
## config
All files that contain the necessary parameters for the proper operation of the node are stored here.
### config/config.yaml
Config .yaml file in which the parameters are stored under conversion_params. Each parameter has a description so that it is comprehensible what it is supposed to do.

## package.xml
All dependencies of the package are entered here. and basic information about the package. The library used here to include the parameters must be specially installed with the following command:
```
sudo apt install ros-humble-generate-parameter-library-py
```
## setup.py
All executables of the package must be entered here so that they are visible to the ros client.
## test
Contains all the important files for running the tests
## path_conversion
Subfolder in which the executables are stored.
### lane_converter.py
The program first imports the following libraries:

- rclpy: This is a library for ROS2, which is a framework for robotics applications.
- math: This is a library for mathematical functions.
- lanelet_msgs: This is a ROS2 message type for lanelet information.
- geometry_msgs: This is a ROS2 message type for geometric information.
- nav_msgs: This is a ROS2 message type for navigation information.
- Node: This is a class from the rclpy library that represents a ROS2 node.

The program then defines a class called PathPlanning. This class inherits from the Node class and has the following methods:

- __init__(): This method is the constructor for the class. It initializes the node and creates publishers and subscribers.
- quaternionsFromPoints(): This method calculates the quaternion from two points.
- middlePoint(): This method calculates the middle point between two points.
- generate_path(): This method generates a path from the lanelet information.

The program then defines a function called main(). This function is the entry point for the program. It initializes the ROS2 framework, creates a PathPlanning object, and then spins the node.

The generate_path() method first creates a Path message. It then iterates through the lanelet information and calculates the middle point between each pair of consecutive points. It then uses the quaternionsFromPoints() method to calculate the quaternion for each point. Finally, it adds a PoseStamped message to the Path message for each point.

The main() function first initializes the ROS2 framework. It then creates a PathPlanning object and spins the node. The spin() method blocks until the node is terminated.

I hope this explanation is helpful. Please let me know if you have any other questions.